# [coffins.bitbucket.org](http://coffins.bitbucket.org/) #

It's my very own and completely tiny static website that provides me a way to introduce myself and things I either do or done already along with simplified blog and contact information. Sounds great, isn't it?

Despite summary above, website has a set of tools it uses to make itself stylish. It's based on following technologies:

* **Custom tricks** as a front-end customization.
* [ZURB Foundation](http://foundation.zurb.com/) as a front-end framework.
* [Font Awesome](http://fontawesome.io) as a font script.
* [Google Font API](https://developers.google.com/fonts/) as a font script, too.